'use strict';
const JSDOM = require('jsdom').JSDOM
const AXIOS = require('axios').default
const MOMENT = require('moment')

const MENU_URL = 'https://www.studierendenwerk-aachen.de/speiseplaene/ahornstrasse-w-en.html'

const fetchTime = MOMENT()
AXIOS.get(MENU_URL)
  .then(processResponse)
  .catch(err => console.error('ERR: ', err));

function processResponse(response) {
  const dom = new JSDOM(response.data)
  const doc = dom.window.document
  const daysDom = doc.querySelectorAll('.accordion > div')
  const additivesDom = doc.querySelector('#additives')
  const daysParsed = [...daysDom].map(parseDay)
  const daysMap = {}
  daysParsed.forEach(d => {  daysMap[d.date] = d })
  const additives = parseAdditives(additivesDom)


  const result = {
    days: daysMap,
    additives,
    url: MENU_URL,
    fetchedAt: fetchTime.format()
  }

  console.log(JSON.stringify(result))
}

function parseDay(dayDom) {
  const headline = dayDom.querySelector('h3')
  const isActive = headline.classList.contains('active-headline')
  const titleText = headline.querySelector('a').textContent
  const date = MOMENT(titleText, 'dddd, DD.MM.YYYY')
  const menuesDom = dayDom.querySelectorAll('table.menues tr')
  const extrasDom = dayDom.querySelectorAll('table.extras tr')

  const menues = [...menuesDom].map(parseMenuEntry)
  const extras = [...extrasDom].map(parseExtrasEntry)

  return {
    date: date.format('YYYY-MM-DD'),
    label: titleText,
    // today: isActive,
    menu: [...menues],
    extras,
  }
}

function parseMenuEntry(row) {
  const classes = row.classList;
  const labels = [...classes].map(cls => {switch (cls) {
    case 'vegan': return cls
    case 'OLV': return 'olv' // octo-lacto-vegetarian
    case 'Schwein': return 'pork'
    case 'Rind': return 'beef'
    case 'Geflügel': return 'poultry'
    case 'Fisch': return 'fish'
    case 'Lamm': return 'lamb'
    case 'odd': return null
    case 'even': return null
    case 'bg-color': return null
    default: return '?:'+cls
  }}).filter(e => !!e)
  const categoryDom = row.querySelector('.menue-category');
  const priceDom = row.querySelector('.menue-price');
  const descDom = row.querySelector('.expand-nutr')
  const nutrDom = row.querySelector('.nutr-info')

  const category = categoryDom ? categoryDom.textContent : undefined
  const price = priceDom ? priceDom.textContent : undefined
  const description = parseDescription(descDom)
  const nutrition = parseNutrition(nutrDom)

  description.forEach(d => {
    if (d.notes)
      d.notes = d.notes.filter(n => !labels.includes(n))
  })

  return {
    labels,
    category,
    price,
    description,
    nutrition,
  }
}

function parseExtrasEntry(row) {
  const categoryDom = row.querySelector('.menue-category');
  const descDom = row.querySelector('.menue-desc')
  const nutrDom = row.querySelectorAll('.nutr-info')

  const category = categoryDom ? categoryDom.textContent : undefined
  const description = parseDescription(descDom)
  const nutrition = [...nutrDom].map(parseNutrition)
  const count = Math.max(description.length, nutrition.length)

  let options = [];
  for (let i = 0; i < count; i++) {
    options.push({
      description: description[i] || null,
      nutrition: nutrition[i] || null,
    })
  }

  return {
    category,
    options,
  }
}

function parseDescription(descDom) {
  const parts = []
  descDom.childNodes.forEach(node => {
    if (node.nodeName !== '#text') return;
    if (node.textContent.trim() === '') return;

    const textRaw = node.data.replace(/^\s*(\|\s*)|\s*$/g, '')
    const textParts = textRaw.split('|').map(s => s.trim())
    if (textParts.length > 1) {
      textParts
        .slice(0,-1)
        .map(t => ({ text: t }))
        .forEach(p => parts.push(p))
    }
    let part = {
      text: textParts[textParts.length - 1]
    }
    let nextNode = node.nextSibling
    while (nextNode) {
      if (nextNode.nodeName === 'SUP') {
        const oldNotes = part.notes || []
        const newNotes = (nextNode.textContent || '?')
          .split(',')
          .map(s => s.trim())
        part.notes = [...oldNotes, ...newNotes]

        nextNode = nextNode.nextSibling
        continue
      } else if (nextNode.nodeName === '#text'
          && nextNode.textContent.trim() === '') {
        nextNode = nextNode.nextSibling
        continue
      }
      break
    }
    parts.push(part)
  })
  return parts
}

function parseNutrition(nutrDom) {
  if (nutrDom.textContent === '-') return null
  const values = {}
  const container = nutrDom.querySelector('div') || nutrDom
  container.childNodes.forEach(node => {
    if (node.nodeName !== '#text') return;
    const match = node.data.match(/^\s*([^=]*?)\s*=\s*(.*?)\s*$/)
    if (!match) { // should not happen ...
      if (!values['?']) values['?'] = [];
      values['?'].push(node.data)
    }

    const rawName = match[1];
    const rawValue = match[2];
    const name = (s => {switch(s) {
      case 'Brennwert': return 'energy'
      case 'Fett': return 'fat'
      case 'Kohlenhydrate': return 'carbohydrate'
      case 'Eiweiß': return 'protein'
      default: return '?:' + s
    }})(rawName)
    values[name] = rawValue
  })
  return values
}

function parseAdditives(additivesDom) {
  const rawText = additivesDom.textContent
  const values = {}
  rawText
    .split(',')
    .forEach(s => {
      const matches = s
        .trim()
        .replace(/^\s*(with|contains)\s*/, '')
        .match(/^\((.*?)\)\s*(.*?)$/)
      if (!matches) {
        if (!values['_?']) values['_?'] = []
        values['_?'].push(s)
        return
      }
      const symbol = matches[1]
      const text = matches[2]
      values[symbol] = text
    })
  return values
}
