'use strict';
const fs = require('fs')
const path = require('path')
const mustache = require('mustache')
const moment = require('moment')

const dataText = fs.readFileSync(path.join(__dirname, 'menu.json'), 'utf-8')
const tplText = fs.readFileSync(path.join(__dirname, 'mail.mustache'), 'utf-8')
//console.log('tpl: ', tplText)
//console.log('data:', dataText)
const data = JSON.parse(dataText)
const dateParam = process.argv[2] || undefined
const dayData = data.days[moment(dateParam).format('YYYY-MM-DD')]
const usedAdditives = {}
dayData.menu.forEach(m =>
  m.description.forEach(d =>
    (d.notes || []).forEach( n => usedAdditives[n] = true)
  )
)
dayData.extras.forEach(e =>
  e.options.forEach(o =>
    (o.description.notes || []).forEach( n => usedAdditives[n] = true)
  )
)

const view = {
  today: dayData,
  url: data.url,
  fetchedAt: data.fetchedAt,
  additives: Object.keys(data.additives)
    .filter(k => usedAdditives[k])
    .map(k => ({
      symbol: k,
      text: data.additives[k]
    }))
}

const rendered = mustache.render(tplText, view)
console.log(rendered)
